FROM maven:3.5-jdk-11 as BUILD
RUN mvn package
VOLUME /tmp
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
